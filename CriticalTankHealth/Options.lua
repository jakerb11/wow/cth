-- Set interface options default values
function cth.setDefaultOptions()
  if cthConfig.displayTankUpdates == nil then
    cthConfig.displayTankUpdates = true
  end
  
  if cthConfig.enableInParty == nil then
    cthConfig.enableInParty = true
  end
  
  if cthConfig.enableInRaid == nil then
    cthConfig.enableInRaid = true
  end
  if cthConfig.includeTankRoleInRaid == nil then
    cthConfig.includeTankRoleInRaid = false
  end
  if cthConfig.useRaidWarning == nil then
    cthConfig.useRaidWarning = false
  end
  if cthConfig.enableInPvp == nil then
    cthConfig.enableInPvp = false
  end
  
  if cthConfig.announceDeaths == nil then
    cthConfig.announceDeaths = true;
  end
  
  if cthConfig.announcePrivately == nil then
    cthConfig.announcePrivately = false
  end
  
  if cthConfig.triggerType == nil then
    cthConfig.triggerType = "percent"
  end
  if cthConfig.triggerPercentThreshold == nil then
    cthConfig.triggerPercentThreshold = 25
  end
  if cthConfig.triggerValueThreshold == nil then
    cthConfig.triggerValueThreshold = 4000
  end
end

-- Create interface options panel
--   Interface panel is not setup until saved variables are loaded
function cth.interfaceOptions()
  local configPanel = CreateFrame("Frame", nil, InterfaceOptionsFramePanelContainer)
  configPanel.name = "Critical Tank Health"
  
  local category = Settings.RegisterCanvasLayoutCategory(configPanel, configPanel.name)
  Settings.RegisterAddOnCategory(category)
  
  -- Setup slash command
  SLASH_CTH1 = "/cth"
  SlashCmdList["CTH"] = function(arg)
    Settings.OpenToCategory(category:GetID());
  end

  -- Configuration panel title
  local title = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalLarge")
  title:SetPoint("TOPLEFT", 16, -16)
  title:SetText("Critical Tank Health")
  
  -- Display tank updates in chat checkbox
  local displayTankUpdatesCheckButton = CreateFrame("CheckButton", nil, configPanel, "InterfaceOptionsCheckButtonTemplate")
  displayTankUpdatesCheckButton:SetPoint("TOPLEFT", title, "BOTTOMLEFT", 0, -16)
  displayTankUpdatesCheckButton.Text:SetText("Display Tank Updates")
  displayTankUpdatesCheckButton.tooltipText = "Select to display tank updates in chat window"
  displayTankUpdatesCheckButton:SetChecked(cthConfig.displayTankUpdates)
  displayTankUpdatesCheckButton:SetScript("OnClick", function(self, button, ...)
    cthConfig.displayTankUpdates = displayTankUpdatesCheckButton:GetChecked()
  end)
  
  -- Enable in party checkbox
  local enableInPartyCheckButton = CreateFrame("CheckButton", nil, configPanel, "InterfaceOptionsCheckButtonTemplate")
  enableInPartyCheckButton:SetPoint("TOPLEFT", displayTankUpdatesCheckButton, "BOTTOMLEFT", 0, -16)
  enableInPartyCheckButton.Text:SetText("Enable in Party")
  enableInPartyCheckButton.tooltipText = "Select to enable CTH while in a party"
  enableInPartyCheckButton:SetChecked(cthConfig.enableInParty)
  enableInPartyCheckButton:SetScript("OnClick", function(self, button, ...)
    cthConfig.enableInParty = enableInPartyCheckButton:GetChecked()
    cth.toggleAddOn()
  end)
  
  -- Enable in raid checkbox
  local enableInRaidCheckButton = CreateFrame("CheckButton", nil, configPanel, "InterfaceOptionsCheckButtonTemplate")
  enableInRaidCheckButton:SetPoint("TOPLEFT", enableInPartyCheckButton, "BOTTOMLEFT", 0, -16)
  enableInRaidCheckButton.Text:SetText("Enable in Raid")
  enableInRaidCheckButton.tooltipText = "Select to enable CTH while in a raid group"
  enableInRaidCheckButton:SetChecked(cthConfig.enableInRaid)
  enableInRaidCheckButton:SetScript("OnClick", function(self, button, ...)
    cthConfig.enableInRaid = enableInRaidCheckButton:GetChecked()
    cth.toggleAddOn()
  end)
  
  -- Include tank role in raid groups checkbox
  local includeTankRoleInRaidCheckButton = CreateFrame("CheckButton", nil, configPanel, "InterfaceOptionsCheckButtonTemplate")
  includeTankRoleInRaidCheckButton:SetPoint("TOPLEFT", enableInRaidCheckButton, "TOPRIGHT", 128, 0)
  includeTankRoleInRaidCheckButton.Text:SetText("Include Tank Role")
  includeTankRoleInRaidCheckButton.tooltipText = "Select to include tank roles as tanks in addition to main tanks"
  includeTankRoleInRaidCheckButton:SetChecked(cthConfig.includeTankRoleInRaid)
  includeTankRoleInRaidCheckButton:SetScript("OnClick", function(self, button, ...)
    cthConfig.includeTankRoleInRaid = includeTankRoleInRaidCheckButton:GetChecked()
  end)

  -- Use raid warning channel in raid groups checkbox
  local useRaidWarningCheckButton = CreateFrame("CheckButton", nil, configPanel, "InterfaceOptionsCheckButtonTemplate")
  useRaidWarningCheckButton:SetPoint("TOPLEFT", includeTankRoleInRaidCheckButton, "TOPRIGHT", 128, 0)
  useRaidWarningCheckButton.Text:SetText("Use Raid Warning")
  useRaidWarningCheckButton.tooltipText = "Select to use raid warning for announcements while in a raid group"
  useRaidWarningCheckButton:SetChecked(cthConfig.useRaidWarning)
  useRaidWarningCheckButton:SetScript("OnClick", function(self, button, ...)
    cthConfig.useRaidWarning = useRaidWarningCheckButton:GetChecked()
  end)
  
  -- Enable in PVP checkbox
  local enableInPvpCheckButton = CreateFrame("CheckButton", nil, configPanel, "InterfaceOptionsCheckButtonTemplate")
  enableInPvpCheckButton:SetPoint("TOPLEFT", enableInRaidCheckButton, "BOTTOMLEFT", 0, -16)
  enableInPvpCheckButton.Text:SetText("Enable in PvP")
  enableInPvpCheckButton.tooltipText = "Select to enable CTH in instanced PVP"
  enableInPvpCheckButton:SetChecked(cthConfig.enableInPvp)
  enableInPvpCheckButton:SetScript("OnClick", function(self, button, ...)
    cthConfig.enableInPvp = enableInPvpCheckButton:GetChecked()
    cth.toggleAddOn()
  end)
  
  -- Announce tank deaths checkbox
  local announceDeathsCheckButton = CreateFrame("CheckButton", nil, configPanel, "InterfaceOptionsCheckButtonTemplate")
  announceDeathsCheckButton:SetPoint("TOPLEFT", enableInPvpCheckButton, "BOTTOMLEFT", 0, -16)
  announceDeathsCheckButton.Text:SetText("Announce Deaths")
  announceDeathsCheckButton.tooltipText = "Select to announce tank deaths"
  announceDeathsCheckButton:SetChecked(cthConfig.announceDeaths)
  announceDeathsCheckButton:SetScript("OnClick", function(self, button, ...)
    cthConfig.announceDeaths = announceDeathsCheckButton:GetChecked()
  end)
  
  -- Announce to private chat window checkbox
  local announcePrivatelyCheckButton = CreateFrame("CheckButton", nil, configPanel, "InterfaceOptionsCheckButtonTemplate")
  announcePrivatelyCheckButton:SetPoint("TOPLEFT", announceDeathsCheckButton, "BOTTOMLEFT", 0, -16)
  announcePrivatelyCheckButton.Text:SetText("Announce in Chat Window")
  announcePrivatelyCheckButton.tooltipText = "Select to display announcements privately in chat window"
  announcePrivatelyCheckButton:SetChecked(cthConfig.announcePrivately)
  announcePrivatelyCheckButton:SetScript("OnClick", function(self, button, ...)
    cthConfig.announcePrivately = announcePrivatelyCheckButton:GetChecked()
  end)
  
  -- Threshold type drop down
  local thresholdTypeLabel = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  thresholdTypeLabel:SetPoint("TOPLEFT", announcePrivatelyCheckButton, "BOTTOMLEFT", 0, -24)
  thresholdTypeLabel:SetText("Critical Health Threshold Type")
  local thresholdTypeDropDown = CreateFrame("Frame", nil, configPanel, "UIDropDownMenuTemplate")
  thresholdTypeDropDown:SetPoint("TOPLEFT", thresholdTypeLabel, "BOTTOMLEFT", 0, -8)
  UIDropDownMenu_SetWidth(thresholdTypeDropDown, 112)
  UIDropDownMenu_SetText(thresholdTypeDropDown, cthConfig.triggerType)
  UIDropDownMenu_Initialize(thresholdTypeDropDown, function(frame, level, menuList)
    local info = UIDropDownMenu_CreateInfo()
    info.checked = false
    
    info.func = function(self, arg1, arg2, checked)
      UIDropDownMenu_SetText(frame, arg1)
      cthConfig.triggerType = UIDropDownMenu_GetText(thresholdTypeDropDown)
    end
    
    -- Add health percent option
    info.text = "percent"
    info.arg1 = info.text
    UIDropDownMenu_AddButton(info)
    
    -- Add health value option
    info.text = "value"
    info.arg1 = info.text
    UIDropDownMenu_AddButton(info)
  end)

  -- Trigger percent health Slider
  local thresholdPercentLabel = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  thresholdPercentLabel:SetPoint("TOPLEFT", thresholdTypeDropDown, "BOTTOMLEFT", 0, -24)
  thresholdPercentLabel:SetText("Health Percent Threshold")
  local triggerPercentSlider = CreateFrame("Slider", nil, configPanel, "OptionsSliderTemplate")
  triggerPercentSlider:SetPoint("TOPLEFT", thresholdPercentLabel, "BOTTOMLEFT", 0, -8)
  triggerPercentSlider:SetWidth(112)
  triggerPercentSlider:SetMinMaxValues(5, 50)
  triggerPercentSlider.Low:SetText(5)
  triggerPercentSlider.High:SetText(50)
  triggerPercentSlider:SetValueStep(1)
  triggerPercentSlider:SetObeyStepOnDrag(true)
  triggerPercentSlider:SetValue(cthConfig.triggerPercentThreshold)
  local thresholdPercentValue = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  thresholdPercentValue:SetPoint("TOPLEFT", triggerPercentSlider, "BOTTOMLEFT", 0, -8)
  thresholdPercentValue:SetWidth(112)
  thresholdPercentValue:SetText(triggerPercentSlider:GetValue())
  thresholdPercentValue:SetJustifyH("CENTER")
  triggerPercentSlider:SetScript("OnValueChanged", function(self, value, ...)
    thresholdPercentValue:SetText(value)
    cthConfig.triggerPercentThreshold = triggerPercentSlider:GetValue()
  end)

  -- Trigger percent health Slider
  local thresholdValueLabel = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  thresholdValueLabel:SetPoint("TOPLEFT", thresholdPercentLabel, "TOPRIGHT", 128, 0)
  thresholdValueLabel:SetText("Health Value Threshold")
  local triggerValueSlider = CreateFrame("Slider", nil, configPanel, "OptionsSliderTemplate")
  triggerValueSlider:SetPoint("TOPLEFT", thresholdValueLabel, "BOTTOMLEFT", 0, -8)
  triggerValueSlider:SetWidth(112)
  triggerValueSlider:SetMinMaxValues(1000, 25000)
  triggerValueSlider.Low:SetText(1000)
  triggerValueSlider.High:SetText(25000)
  triggerValueSlider:SetValueStep(500)
  triggerValueSlider:SetObeyStepOnDrag(true)
  triggerValueSlider:SetValue(cthConfig.triggerValueThreshold)
  local thresholdValue = configPanel:CreateFontString(nil, "ARTWORK", "GameFontNormalSmall")
  thresholdValue:SetPoint("TOPLEFT", triggerValueSlider, "BOTTOMLEFT", 0, -8)
  thresholdValue:SetWidth(112)
  thresholdValue:SetText(triggerValueSlider:GetValue())
  thresholdValue:SetJustifyH("CENTER")
  triggerValueSlider:SetScript("OnValueChanged", function(self, value, ...)
    thresholdValue:SetText(value)
    cthConfig.triggerValueThreshold = triggerValueSlider:GetValue()
  end)
end
