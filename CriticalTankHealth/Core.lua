--
-- Data
--

-- Player group type
local groupType = nil
local maxGroupMembers = 0
local chatType = nil

-- Tank table, key is unit identifier, value indicates health criticality
local tanks = {}

-- List of valid healing spells
local healSpells = {
  "Binding Heal",
  "Chain Heal",
  "Divine Light",
  "Greater Heal",
  "Greater Healing Wave",
  "Flash Heal",
  "Flash of Light",
  "Heal",
  "Healing Surge",
  "Healing Touch",
  "Healing Wave",
  "Holy Light",
  "Holy Radiance",
  "Nourish",
  "Prayer of Healing",
  "Regrowth"
}

--
-- Functions
--

local function setGroupType()
  -- Determine group type
  if IsInRaid() then
    groupType = "raid" 
    maxGroupMembers = MAX_RAID_MEMBERS
    chatType = groupType
    if (cthConfig.useRaidWarning) then
      chatType = "raid_warning"
    end
  elseif IsInGroup() then
    groupType = "party"
    maxGroupMembers = MAX_PARTY_MEMBERS + 1
    chatType = groupType
  end
  
  -- Override chat if in instance group
  if IsInGroup(LE_PARTY_CATEGORY_INSTANCE) then
    chatType = "instance_chat"
  end
end

-- Determines if a unit has low health
local function unitHasLowHealth(unit)
  if cthConfig.triggerType == "value" then
    unitHealth = UnitHealth(unit)
    return unitHealth <= cthConfig.triggerValueThreshold and not UnitIsDeadOrGhost(unit)
  else
    unitHealth = cth.unitPercentHealth(unit)
    return unitHealth <= cthConfig.triggerPercentThreshold and not UnitIsDeadOrGhost(unit)
  end
end

-- Determines if a unit is a tank
local function isTank(unit)
  if groupType == "raid" then
    return GetPartyAssignment("MAINTANK", unit, true) or (cthConfig.includeTankRoleInRaid and UnitGroupRolesAssigned(unit) == "TANK")
  else
    return UnitGroupRolesAssigned(unit) == "TANK"
  end
end

-- Returns a table of tanks in the player's group
local function findTanks()
  -- Initialize tank array
  local tanks = {}
  
  -- Find tanks in player's group
  for i = 1, maxGroupMembers do
    unit = groupType .. i
    
    -- There is no party5 -- use player
    if unit == "party5" then
      unit = "player"
    end
    
    if UnitExists(unit) and isTank(unit) then
      tanks[unit] = false
    end
  end
  
  return tanks
end

-- Displays tank list in chat window
local function printTanks()
  tankListStr = ""
  for tank, _ in pairs(tanks) do
    tankListStr = tankListStr .. " " .. cth.unitNameClassColor(tank)
  end
  
  print("<|cFFC74040CTH|r> Tanks: " .. tankListStr)
end

-- Updates the add on tank table
local function updateTanks()
  local newTanks = findTanks()
  
  -- Check if any tanks removed
  for tank, _ in pairs(tanks) do
    if newTanks[tank] == nil then
      tanks = newTanks
      if cthConfig.displayTankUpdates then
        printTanks()
      end
      return
    end
  end
  
  -- Check if any tanks added
  for tank, _ in pairs(newTanks) do
    if tanks[tank] == nil then
      tanks = newTanks
      if cthConfig.displayTankUpdates then
        printTanks()
      end
      return
    end
  end
end

-- Find (if any) the next heal spell being cast (not necessarily on tank -- that is impossible to detect)
local function findHeal()
  -- Assume no heal incoming
  local nextHealSpell = nil
  local timeUntilNextHeal = nil
  
  -- Iterate over group members
  for i = 1, maxGroupMembers do
    local unit = groupType .. i
    
    -- There is no party5 -- use player
    if unit == "party5" then
      unit = "player"
    end
    
    -- Get spell being cast by group member
    local spell, _, _, _, endTime, _, _, _, spellId = UnitCastingInfo(unit)
    
    -- If spell is a heal
    for _, healSpell in pairs(healSpells) do
      if (spell == healSpell) then
        -- Duration left on cast
        local timeUntilCast = endTime / 1000 - GetTime()
        -- Check if this is the next heal to go off
        if (not timeUntilNextHeal or timeUntilCast < timeUntilNextHeal) then
          nextHealSpell = spell
          nextHealSpellId = spellId
          timeUntilNextHeal = timeUntilCast
          break
        end
      end
    end
  end
  
  return nextHealSpell, nextHealSpellId, timeUntilNextHeal
end

-- Send chat message report for low health unit
local function announceUnitLowHealth(unit)
  -- Build health message
  local healthStr = nil
  if cthConfig.triggerType == "value" then
    healthStr = UnitHealth(unit)
  else
    healthStr = string.format("%.0f", cth.unitPercentHealth(unit)) .. "%"
  end
  
  -- Build heal spell message
  local healStr = "NO INCOMING HEAL"
  local healSpell, healSpellId, timeUntilCast = findHeal()
  if (healSpell) then
    healStr = GetSpellLink(healSpellId) .. " in " .. string.format("%.1f", timeUntilCast) .. "s"
  end
  
  if cthConfig.announcePrivately then
    healthStr = "|cFFFF6600" .. healthStr .. "|r"
    healStr = "|cFF00CC00" .. healStr .. "|r"
    print("<|cFFC74040CTH|r> " .. cth.unitNameClassColor(unit) .. " " .. healthStr .. " " .. healStr)
  else
    SendChatMessage("<CTH> " .. UnitName(unit) .. " " .. healthStr .. " " .. healStr, chatType)
  end
end

-- Send chat message report unit death
local function announceUnitDeath(unit)
  if cthConfig.announcePrivately then
    print("<|cFFC74040CTH|r> " .. cth.unitNameClassColor(unit) .. " HAS DIED")
  else
    SendChatMessage("<CTH> " .. UnitName(unit) .. " HAS DIED", chatType)
  end
end

--
-- Event Frame
--

-- Main frame
local frame = CreateFrame("Frame")
-- Events array
local events = {}

--
-- Event handlers
--

-- Enable add on functionality
local function enableAddOn()
  setGroupType()
  
  frame:RegisterEvent("GROUP_ROSTER_UPDATE")
  frame:RegisterEvent("UNIT_HEALTH")
  
  if cthConfig.announceDeaths then
    frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
  end
  
  events["GROUP_ROSTER_UPDATE"]()
end

-- Disable addon functionality
local function disableAddOn()
  frame:UnregisterEvent("GROUP_ROSTER_UPDATE")
  frame:UnregisterEvent("UNIT_HEALTH")
  frame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
end

-- Enables or disables add on functionality as appropriate
function cth.toggleAddOn()
  if cth.isInPvpInstance() then
    if cthConfig.enableInPvp then
      enableAddOn()
      return
    end
  elseif IsInRaid() then
    if cthConfig.enableInRaid then
      enableAddOn()
      return
    end
  elseif IsInGroup() then
    if cthConfig.enableInParty then
      enableAddOn()
      return
    end
  end
  
  disableAddOn()
end

-- Add On loaded initialization event
events["ADDON_LOADED"] = function(addOnName)
  if addOnName == "CriticalTankHealth" then
    print("<|cFFC74040CTH|r> loaded (/cth)")
  end
end

-- Saved variables loaded event
events["VARIABLES_LOADED"] = function()
  -- Set default options after saved variables have been loaded
  cth.setDefaultOptions()
  -- Create interface options panel after saved variables loaded
  cth.interfaceOptions()
  cth.toggleAddOn()
end

-- Group joined event
events["GROUP_JOINED"] = function()
  cth.toggleAddOn()
end

-- Group left event
events["GROUP_LEFT"] = function()
  disableAddOn()
end

-- Group roster update event handler
events["GROUP_ROSTER_UPDATE"] = function(...)
  setGroupType()
  updateTanks()
end

-- Unit health update event handler
events["UNIT_HEALTH"] = function(unit)
  -- Require combat
  if not UnitAffectingCombat("player") then
    return
  end
  
  -- Check tank health
  for tank, isCritical in pairs(tanks) do
    if unit == tank
    then
      if not unitHasLowHealth(unit) then
        -- Tank is above critical health threshold
        tanks[unit] = false
      elseif not isCritical then
        -- Tank is now below critical health threshold
        tanks[unit] = true
        announceUnitLowHealth(unit)
      end
    end
  end
end

-- Combat log event handler (to detect tank death)
events["COMBAT_LOG_EVENT_UNFILTERED"] = function(...)
  local _, subEvent, _, _, _, _, _, _, destName = CombatLogGetCurrentEventInfo()
  if subEvent == "UNIT_DIED" then
    -- Check if dead unit is a tank
    for tank, _ in pairs(tanks) do
      if destName == UnitName(tank) then
        -- Tank has died
        tanks[tank] = true;
        announceUnitDeath(tank)
      end
    end
  end
end

-- Register events
frame:RegisterEvent("ADDON_LOADED")
frame:RegisterEvent("VARIABLES_LOADED")
frame:RegisterEvent("GROUP_JOINED")
frame:RegisterEvent("GROUP_LEFT")

-- Main event handler
local function eventHandler(self, event, ...)
  events[event](...)
end

-- Set frame script event handler
frame:SetScript("OnEvent", eventHandler)
