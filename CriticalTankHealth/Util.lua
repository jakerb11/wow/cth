--
-- Data
--

-- Table of class colors
local classColors = {
  DEATHKNIGHT = "FFC41F3B",
  DRUID       = "FFFF7D0A",
  HUNTER      = "FFABD473",
  MAGE        = "FF69CCF0",
  PALADIN     = "FFF58CBA",
  PRIEST      = "FFFFFFFF",
  ROGUE       = "FFFFF569",
  SHAMAN      = "FF0070DE",
  WARLOCK     = "FF9482C9",
  WARRIOR     = "FFC79C6E",
}

--
-- Functions
--

-- Calculates unit percent health
function cth.unitPercentHealth(unit)
  return (UnitHealth(unit) / UnitHealthMax(unit)) * 100
end

-- Returns the unit's name formatted in its class color
function cth.unitNameClassColor(unit)
  return "|c" .. classColors[UnitClassBase(unit)] .. UnitName(unit) .. "|r"
end

-- Determines if player is in PVP instance
function cth.isInPvpInstance()
  local _, instanceType = IsInInstance()
  return instanceType == "pvp" or instanceType == "arena"
end
