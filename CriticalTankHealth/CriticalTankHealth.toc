## Interface: 40400
## Title: Critical Tank Health
## Notes: Sends chat message reports when a tank is critically low on health
## SavedVariables: cthConfig

Init.lua
Util.lua
Options.lua
Core.lua
