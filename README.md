# Critical Tank Health

Critical Tank Health (CriticalTankHealth or CTH) is an add-on that detects
tanks in a group and reports low health and death messages to the chat
window.

## Download/Installation Instructions

1. Make sure you have the main branch selected
2. Click the download button
3. Download as a .zip file (cth-main.zip)
4. Extract the files somewhere
5. Navigate to the directory called CriticalTankHealth
6. Copy or move the CriticalTankHealth directory to you WoW addons folder

## Interface Options

The CTH options window can be found in the in-game user interface panel.  The
slash command, /cth, can also be used to access the user options panel.

### Display Tank Updates

The display tank updates check box toggles the displaying of the list of
detected tanks.  When enabled, the current tanks are listed in a chat message
only you can see.  When disabled,  no message is printed.

### Enable In Party

This check box enables/disables the add-on when in a party group.

### Enable in Raid

This check box enables/disables the add-on when in a raid group.

### Include Tank Role

When selected, this option includes players with the tank role selected as
tanks in addition to those marked as main tank.  When unselected, only players
marked as main tank are counted.

### Use Raid Warning

When selected, use the raid warning channel to make announcements when in a
raid group.

### Enable in PvP

This check box enables/disables the add-on when in instance PvP.

### Announce Deaths

When selected announce tank deaths in addition to tank low health.

### Announce in Chat Window

When selected announcements will only be visible in the player's own chat
window.

### Critical Health Threshold Type

Select between percent and value.  When set to percent, the critical tank
health threshold is precent health based.  When se to value, the critical
tank health threshold is health value based.

### Trigger Sliders

The two sliders are used to select the critical tank health percentage
and value threshold.
